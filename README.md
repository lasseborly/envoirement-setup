# Envoirement Setup
Both the server and client setup is contained in the Makefile. This might change further down the line.
The idea is that a stock Ubuntu 14.04.3 can be deployed and with this single Makefile have a development and production setup ready.

Nothing needed to be explained other than `git` and `make` has to be installed.

* `sudo apt-get install -y git`
* `sudo git clone https://lasseborly@bitbucket.org/lasseborly/envoirement-setup.git`

And that should do it! Now just run `make` and whatever setup you want!
