default: client-setup

client-setup: theme fonts development ide apps

server-setup: development

theme:
	gsettings get org.gnome.desktop.background picture-uri
	gsettings set org.gnome.desktop.background primary-color '#2c3e50'
	gsettings set org.gnome.desktop.background secondary-color '#2c3e50'
	sudo add-apt-repository -y ppa:numix/ppa
	sudo apt-get update
	sudo apt-get install -y numix-gtk-theme numix-icon-theme-circle
	gsettings set org.gnome.desktop.interface gtk-theme "Numix"
	gsettings set org.gnome.desktop.interface icon-theme "Numix-Circle"
	gsettings set org.gnome.desktop.wm.preferences theme "Numix"

fonts:
	sudo apt-get install -y fonts-lato fonts-inconsolata
	gsettings set org.gnome.desktop.interface document-font-name "Lato Light 11"
	gsettings set org.gnome.desktop.interface font-name "Lato 11"
	gsettings set org.gnome.desktop.interface monospace-font-name "Inconsolata Bold 13"
	gsettings set org.gnome.nautilus.desktop font "Lato Bold 11"

apps:
	sudo wget -c https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
	sudo dpkg --install google-chrome-stable_current_amd64.deb
	sudo rm google-chrome-stable_current_amd64.deb
	sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys BBEBDCB318AD50EC6865090613B00F1FD2C19886
	echo deb http://repository.spotify.com stable non-free | sudo tee /etc/apt/sources.list.d/spotify.list
	sudo apt-get update
	sudo apt-get install -y spotify-client deluge vlc gimp
	sudo wget -c https://github.com/Aluxian/Facebook-Messenger-Desktop/releases/download/v1.4.3/Messenger_linux64.deb
	sudo dpkg --install Messenger_linux64.deb
	sudo rm Messenger_linux64.deb
	sudo wget -c https://edgehill.nylas.com/download?platform=linux-deb&_ga=1.187292203.2095056190.1453773983
	sudo dpkg --install N1.deb
	sudo rm N1.deb

development:
	sudo apt-get install -y git
	sudo apt-get install -y curl
	curl -sL https://deb.nodesource.com/setup_5.x | sudo -E bash -
	sudo apt-get install -y nodejs
	sudo npm install -g npm
	sudo npm install -g nodemon
	sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
	echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list
	sudo apt-get update
	sudo apt-get install -y mongodb-org

ide:
	sudo wget -c https://atom.io/download/deb
	sudo dpkg --install deb
	sudo rm deb
	sudo apm install pigments minimap merge-conflicts color-picker file-icons git-plus
